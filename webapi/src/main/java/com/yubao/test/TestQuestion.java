package com.yubao.test;

import com.alibaba.fastjson.JSON;
import com.yubao.Appllication;
import com.yubao.request.AddQuestionReq;
import com.yubao.request.QueryAnswersOfQuestionReq;
import com.yubao.request.QueryQuestionsByUser;
import com.yubao.response.AnswerViewModel;
import com.yubao.response.PageObject;
import com.yubao.response.QuestionViewModel;
import com.yubao.response.UserViewModel;
import com.yubao.service.LoginService;
import com.yubao.service.QuestionService;
import com.yubao.util.NormalException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Appllication.class)
public class TestQuestion {

    @MockBean
    LoginService userInfoService;

    @Autowired
    QuestionService questionService;

    private static final Logger LOGGER = LoggerFactory.getLogger(TestQuestion.class);

    @Before
    public void setUp() throws NormalException {
        UserViewModel mockuser = new UserViewModel();
        mockuser.setName("admin");
        mockuser.setId("7e7c70a1-f882-43b9-8504-743a90a4d294");
        mockuser.setRmb(2);
        when(userInfoService.get()).thenReturn(mockuser);

    }

    @Test
    public void add() throws Exception {

        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH_mm_ss_SSS");

        AddQuestionReq req = new AddQuestionReq();
        req.setContent("content" + simpleDateFormat1.format(new Date()));
        req.setTitle("test" + simpleDateFormat1.format(new Date()));
        questionService.add(req);

    }

    @Test
    public void getByUserAnswer() throws NormalException {
        QueryQuestionsByUser req = new QueryQuestionsByUser();
        req.setUid("7e7c70a1-f882-43b9-8504-743a90a4d294");
        req.setIndex(1);
        req.setSize(10);
        PageObject<QuestionViewModel> result = questionService.getByUserAnswer(req);
        LOGGER.info(JSON.toJSONString(result));
    }



    @Test
    public void queryAnswersOfQuestion() throws NormalException {
        QueryAnswersOfQuestionReq req = new QueryAnswersOfQuestionReq();
        req.setJid("feb4a875-c97d-4b9e-9493-5672cb928bac");
        req.setIndex(1);
        req.setSize(10);
        PageObject<AnswerViewModel> result = questionService.queryAnswersOfQuestion(req);
        LOGGER.info(JSON.toJSONString(result));
    }




}
