package com.yubao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubao.entity.Message;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageMapper extends BaseMapper<Message> {

    void deleteByUser(String userid);

}