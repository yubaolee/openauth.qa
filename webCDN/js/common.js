/*
 * @Author: yubaolee <yubaolee@163.com> | ahfu~ <954478625@qq.com>
 * @Date: 2024-06-19 20:05:38
 * @LastEditTime: 2024-06-19 20:13:16
 * @Description: 
 * @
 * @Copyright (c) 2024 by yubaolee | ahfu~ , All Rights Reserved. 
*/
const  Message = new Vue().$message
const service = axios.create({
    baseURL:'http://demo.openauth.net.cn:8889', // api 的 base_url
    timeout: 25000 ,// 请求超时时间
    headers: {
        'Content-type': 'application/json;charset=UTF-8'
    },
  })
  
  // request拦截器
  service.interceptors.request.use(
    config => {
    	config.headers['X-Token'] = sessionStorage.token
        return config
    },
    error => {
      // Do something with request error
      console.log(error) // for debug
      Promise.reject(error)
    }
  )
  
  // response 拦截器
  service.interceptors.response.use(
    response => {
      /**
       * code为非20000是抛错 可结合自己业务进行修改
       */
      const res = response.data
      return response.data
    },
    error => {
      console.log('err' + error) // for debug
      Message({
        message: error.message,
        type: 'error',
        duration: 5 * 1000
      })
      return Promise.reject(error)
    }
  )
  
//获取url中的字段
function GetQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if(r != null) return unescape(r[2]);
	return null;
}

var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?93a7b9a145222f9b7109d643a0c58f8d";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();


