/*
 * @Author: yubaolee <yubaolee@163.com> | ahfu~ <954478625@qq.com>
 * @Date: 2021-06-22 19:25:00
 * @LastEditTime: 2024-06-19 20:13:42
 * @Description: 
 * @
 * @Copyright (c) 2023 by yubaolee | ahfu~ , All Rights Reserved. 
*/
Vue.component('app-footer', {
    template: `
        <div class="box-flex align-center text-center flex-column">
            <div>2024 © <a href="#" class="p-l-m p-r-m">openauth.net.cn</a>LGPL license 官方技术交流群：484498493 【已满】<span class="p-l-m p-r-m">626433139【已满】</span>566344079 </div>
            <div>
                <a class="p-r-sm" href="http://www.openauth.net.cn/question/detail.html?id=a2be2d61-7fcb-4df8-8be2-9f296c22a89c">商务合作/授权说明</a>
                <a class="p-l-sm" href="https://gitee.com/yubaolee/OpenAuth.Core" target="_blank">Git仓库</a>
                <span class="p-l-sm p-r-sm">|</span>
                <a href="http://www.beian.miit.gov.cn" target="_blank">渝ICP备16009992号-1</a>
            </div>
        </div>
    `
})